/* ----- Our Services Section: ----- */
$(document).ready(function () {
    const ourServicesTabCaptItems = $(`.our-services .tabs-caption-item`); // console.log(ourServicesTabCaptionCollection);
    const ourServicesTabContentItems = $(`.our-services .tabs-content-item`); // console.log(ourServicesTabContentCollection);
    let ourServiceTabIndex = 0;

    showActiveInfo(ourServiceTabIndex, ourServicesTabCaptItems, ourServicesTabContentItems);

    for (let i = 0; i < ourServicesTabCaptItems.length; i++) {
        ourServicesTabCaptItems.eq(i).click(function () {
            showActiveInfo(i, ourServicesTabCaptItems, ourServicesTabContentItems);
        });
    }
});

/* ----- Our Amazing Work Section: ----- */
$(document).ready(function () {
    const amazingTabsCollection = $(`.our-amazing-work .tabs-caption-item`);
    const amazingContentCollection = $(`.our-amazing-work .tabs-content-item`); //console.log(ourAmazingContentItems);
    const buttonLoadAmazing = $(`.our-amazing-work .button-load`);
    const spinner = $(`.spinner-wrap`);
    let timerId = 0;

    for (let i = 0; i < amazingContentCollection.length; i++) {
        amazingContentCollection.eq(i).addClass(`amazing`);
    }

    amazingContentCollection.hide();
    amazingContentCollection.slice(0, 12).fadeIn(500);

    spinner.hide();
    buttonLoadAmazing.data(`filter`, `general`);

    buttonLoadAmazing.click(function () {
        if (buttonLoadAmazing.data(`filter`) === `general`) {
            buttonLoadAmazing.hide();
            spinner.show();
            clearTimeout(timerId);

            timerId = setTimeout(function () {
                spinner.hide();
                $(`.amazing:hidden`).slice(0, 12).fadeIn(800);
                if ($(`.amazing:hidden`).length > 0) {
                    buttonLoadAmazing.show();
                } else {
                    buttonLoadAmazing.hide();
                }
            }, 2000);
        }
    });

    for (let i = 0; i < amazingTabsCollection.length; i++) {
        amazingTabsCollection.eq(i).click(function () {
            amazingTabsCollection.eq(i).addClass(`active`);
            amazingTabsCollection.eq(i).siblings().removeClass(`active`);
            buttonLoadAmazing.show(); // воскрешаем кнопку загрузки картинок
            buttonLoadAmazing.data(`filter`, `${amazingTabsCollection.eq(i).data('filter')}`);// добавляем кнопке один нужный операционный класс, например, graphic
            amazingContentCollection.hide(); // прячем все картинки секции
            amazingContentCollection.filter(`.amazing.${amazingTabsCollection.eq(i).data('filter')}`).slice(0, 4).fadeIn(500); // отфильтровываем нужные картинки и показываем только 4 из этого отфильтрованного количесвта

            buttonLoadAmazing.click(function () {
                if (buttonLoadAmazing.data(`filter`) === `${amazingTabsCollection.eq(i).data('filter')}`) {
                    buttonLoadAmazing.hide();
                    spinner.show();
                    clearTimeout(timerId);

                    timerId = setTimeout(function () {
                        spinner.hide();
                        $(`.${amazingTabsCollection.eq(i).data('filter')}:hidden`).slice(0, 4).fadeIn(500);
                        if ($(`.${amazingTabsCollection.eq(i).data('filter')}:hidden`).length > 0) {
                            buttonLoadAmazing.show();
                        } else {
                            buttonLoadAmazing.hide();
                        }
                    }, 1000);
                }
            });

            if (buttonLoadAmazing.data(`filter`) === `all`) {
                buttonLoadAmazing.hide();
                spinner.show();
                clearTimeout(timerId);

                timerId = setTimeout(function () {
                    spinner.hide();
                    amazingContentCollection.filter(`.amazing`).fadeIn(1000);
                    if ($(`.${amazingTabsCollection.eq(i).data('filter')}:hidden`).length > 0) {
                        buttonLoadAmazing.show();
                    } else {
                        buttonLoadAmazing.hide();
                    }
                }, 2000);
            }
        });
    }
});

/* ----- What People Say Section: ----- */

// $(document).ready(function () {
    // const btnPrev = $(`.button-previous`);
    // const btnNext = $(`.button-next`);
    // const wrapAllThumbs = $(`.wrapAllThumbs`);
    // const thumbsCollection = $(`.thumb`); // console.log(thumbsCollection);
    // let timeout = 150;
    // const step = 115;
    // let slidecount = 0; //Math.floor(Math.random() * thumbsCollection.length);
    //
    // showActiveInfo(slidecount, thumbsCollection, $(`.person-content-item`));
    //
    // btnNext.click(function () {
    //     slidecount++;
    //     if (parseFloat(wrapAllThumbs.css('right')) <= -100) {
    //         showActiveInfo(slidecount, thumbsCollection, $(`.person-content-item`));
    //         wrapAllThumbs.animate({left: `-=${step}px`}, timeout);
    //     }
    //
    //     if (parseFloat(wrapAllThumbs.css('right')) > -100 && slidecount < thumbsCollection.length) {
    //         showActiveInfo(slidecount, thumbsCollection, $(`.person-content-item`));
    //     }
    //
    //     if (slidecount >= thumbsCollection.length) {
    //         slidecount = 0;
    //         showActiveInfo(slidecount, thumbsCollection, $(`.person-content-item`));
    //         wrapAllThumbs.animate({left: `0`}, timeout);
    //     }
        // console.log(wrapAllThumbs.position().left, slidecount);
    // });
    //
    // btnPrev.click(function () {
    //     slidecount--;
    //     if (parseFloat(wrapAllThumbs.css('left')) < 0) {
    //         showActiveInfo(slidecount, thumbsCollection, $(`.person-content-item`));
    //         wrapAllThumbs.animate({left: `+=${step}px`}, timeout);
    //     }
    //
    //     if (parseFloat(wrapAllThumbs.css('left')) >= 0 && slidecount >= 0) {
    //         showActiveInfo(slidecount, thumbsCollection, $(`.person-content-item`));
    //     }
    //
    //     if (slidecount < 0) {
    //         slidecount = thumbsCollection.length - 1;
    //         showActiveInfo(slidecount, thumbsCollection, $(`.person-content-item`));
    //         wrapAllThumbs.animate({left: `-${step * (thumbsCollection.length - 4)}px`}, timeout);
    //     }
    // });
    //
    // for (let i = 0; i < thumbsCollection.length; i++) {
    //     thumbsCollection.eq(i).click(function () {
    //         showActiveInfo(i, thumbsCollection, $(`.person-content-item`));
    //         slidecount = i;
    //     });
    // }
// });


/*Gallery Of Best Images. Masonry Section:*/
$(document).ready(function () {
    let $grid = $('.grid-container');
    let $grid2 = $('.grid-container-2');
    let $grid3 = $('.grid-container-3');

    const gallerySpinner = $(`.gallery .spinner-wrap`); //console.log(galleryCollection);
    const galleryButtonLoad = $(`.gallery .button-load`);
    let timerId = 0;

    // $grid.imagesLoaded(callMasonry($grid, `.grid-item`, '.percent-width','.gutter-size')); // применение функции callMasonry() в этом месте ломает код

    $grid.imagesLoaded(function () { // init Masonry after all images have loaded
        $grid.masonry({ // options...
            itemSelector: '.grid-item',
            columnWidth: '.percent-width',
            percentPosition: true,
            gutter: '.gutter-size',
            horizontalOrder: true
        });
    });

    $grid2.imagesLoaded(callMasonry($grid2, `.grid-item-2`, '.percent-width-2','.gutter-size-2'));
    $grid3.imagesLoaded(callMasonry($grid3, `.grid-item-3`, '.percent-width-4','.gutter-size-2'));

    $(`.grid-item`).hide();
    $(`.grid-item:hidden`).slice(0, 8).show();

    galleryButtonLoad.click(function () {
        galleryButtonLoad.hide();
        gallerySpinner.show();
        clearTimeout(timerId);
        timerId = setTimeout(function () {
            gallerySpinner.hide();
            $(`.grid-item:hidden`).slice(0, 9).fadeIn(800);

            $grid.imagesLoaded(callMasonry($grid, `.grid-item`, '.percent-width','.gutter-size'));

            if ($(`.grid-item:hidden`).length > 0) {
                galleryButtonLoad.show();
            } else {
                galleryButtonLoad.hide();
            }
        }, 3000);
    });
});

function showActiveInfo(index, tabCollection, contentCollection) {
    contentCollection.hide();
    contentCollection.eq(index).fadeIn(500);

    tabCollection.removeClass(`active`);
    tabCollection.eq(index).addClass(`active`);
}

function callMasonry(gridName, itemSelector, columnWidth, horizontalGap) { // init Masonry after all images have loaded
    gridName.masonry({ // options...
        itemSelector: itemSelector,//'.grid-item',
        columnWidth: columnWidth,//'.percent-width',
        percentPosition: true,
        gutter: horizontalGap,//'.gutter-size',
        horizontalOrder: true
    });
}