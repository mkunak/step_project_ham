$(document).ready(function () {
    $('.slider-content').slick(
    {
        arrows: false,
        fade: true,
        initialSlide: 1,
        speed: 500,
        swipe: true,
        asNavFor: '.slider-nav'
    });

    $('.slider-nav').slick({
        prevArrow: '.button-previous',
        nextArrow: '.button-next',
        slidesToShow: 4,
        initialSlide: 1,
        slidesToScroll: 1,
        centerPadding: '10px',
        speed: 500,
        asNavFor: '.slider-content',
        centerMode: true,
        focusOnSelect: true
    });
});

// $(document).ready(function () {
    // const btnPrev = $(`.button-previous`);
    // const btnNext = $(`.button-next`);
    // const wrapAllThumbs = $(`.wrapAllThumbs`);
    // const thumbsCollection = $(`.thumb`); // console.log(thumbsCollection);
    // let timeout = 150;
    // const step = 115;
    // let slidecount = 0; //Math.floor(Math.random() * thumbsCollection.length);
    //
    // showActiveInfo(slidecount, thumbsCollection, $(`.personContentItem`));
    //
    // btnNext.click(function () {
    //     slidecount++;
        //     if (parseFloat(wrapAllThumbs.css('right')) <= -100) {
        //         showActiveInfo(slidecount, thumbsCollection, $(`.personContentItem`));
        //         wrapAllThumbs.animate({left: `-=${step}px`}, timeout);
        //     }
        //
        //     if (parseFloat(wrapAllThumbs.css('right')) > -100 && slidecount < thumbsCollection.length) {
        //         showActiveInfo(slidecount, thumbsCollection, $(`.personContentItem`));
        //     }
        //
        //     if (slidecount >= thumbsCollection.length) {
        //         slidecount = 0;
        //         showActiveInfo(slidecount, thumbsCollection, $(`.personContentItem`));
        //         wrapAllThumbs.animate({left: `0`}, timeout);
        //     }
        // console.log(wrapAllThumbs.position().left, slidecount);
    // });
    //
    // btnPrev.click(function () {
    //     slidecount--;
    //     if (parseFloat(wrapAllThumbs.css('left')) < 0) {
    //         showActiveInfo(slidecount, thumbsCollection, $(`.personContentItem`));
    //         wrapAllThumbs.animate({left: `+=${step}px`}, timeout);
    //     }
    //
    //     if (parseFloat(wrapAllThumbs.css('left')) >= 0 && slidecount >= 0) {
    //         showActiveInfo(slidecount, thumbsCollection, $(`.personContentItem`));
    //     }
    //
    //     if (slidecount < 0) {
    //         slidecount = thumbsCollection.length - 1;
    //         showActiveInfo(slidecount, thumbsCollection, $(`.personContentItem`));
    //         wrapAllThumbs.animate({left: `-${step * (thumbsCollection.length - 4)}px`}, timeout);
    //     }
    // });
    //
    // for (let i = 0; i < thumbsCollection.length; i++) {
    //     thumbsCollection.eq(i).click(function () {
    //         showActiveInfo(i, thumbsCollection, $(`.personContentItem`));
    //         slidecount = i;
    //     });
    // }
// });